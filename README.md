# Eurhisfirm-SiteLinks-Server

_Web service providing a MediaWiki-compatible API for sitelinks_

## Installation & Configuration

```bash
git clone https://gitlab.huma-num.fr/eurhisfirm/eurhisfirm-sitelinks-server.git
cd eurhisfirm-sitelinks-server/
npm install
```

## Running Server...

### ... in development mode

```bash
npm run dev
```

### ... in production mode

```bash
npm run build
```
